package main;

import java.io.IOException;

import search.Searcher;

import compilation.Analyser;
import compilation.Connecter;

public class MainClass {
	
	/*
	 * Author: Valentin Macheret, SCIA 2015
	 * Bonus 1 & 2 are in DataStore class.
	 */
	private static final boolean COMPILATION = false;
	
	public static void main(String[] args) {
		if (COMPILATION) {
			compile();
		} else {
			search();
		}
	}
	
	private static void search() {
		Searcher lSearcher = new Searcher();
		lSearcher.launchSearch("pony"); // ;D
		lSearcher.launchSearch("common and subjects");
		lSearcher.launchSearch("common and subjects and injectable");
		lSearcher.launchSearch("common and subjects and takazoumeupabo");
	}
	
	private static void compile() {
		try {
			Connecter lConnector = new Connecter();
			lConnector.listFilesInFolder("resources");
			Analyser lAnalysor = new Analyser();
			lAnalysor.processTokenization(lConnector.getContents());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
