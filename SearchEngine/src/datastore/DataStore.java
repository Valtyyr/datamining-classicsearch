package datastore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DataStore {

	public static void saveOnDisk(Map<String, List<String>> iIndexation) {
        FileOutputStream lFos;
		try {
			lFos = new FileOutputStream("dictionnary/dic.bin");
			ObjectOutputStream lOos = new ObjectOutputStream(lFos);
			lOos.writeObject(iIndexation);
			lOos.close();
			System.out.println("Build of the dictionnary : SUCCESSFUL");
		} catch (IOException e) {
			System.out.println("Build of the dictionnary : FAIL");
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, List<String>> loadIndexation() {
		Map<String, List<String>> lIndexation = new HashMap<String, List<String>>();
		try {
			FileInputStream lFis = new FileInputStream("dictionnary/dic.bin");
			ObjectInputStream lOis;
			lOis = new ObjectInputStream(lFis);
			lIndexation = (Map<String, List<String>>) lOis.readObject();
			lOis.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return lIndexation;
	}
	
	/*
	 * Bonus 1
	 */
	public static void printMetaData(String iName) throws IOException {
		Map<String, List<String>> lIndexation = loadIndexation();
		for (Entry<String, List<String>> lEntry : lIndexation.entrySet()) {
			for (String lFile : lEntry.getValue()) {
				if (lFile.equals(iName)) {
					Path lPath = FileSystems.getDefault().getPath(iName);
					BasicFileAttributes lAttributes = Files.readAttributes(lPath, BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
					System.out.println(lAttributes.creationTime().toString());
					System.out.println(lAttributes.lastAccessTime().toString());
					System.out.println(lAttributes.lastModifiedTime().toString());
					System.out.println(String.valueOf(lAttributes.isDirectory()));
					System.out.println(String.valueOf(lAttributes.isOther()));
					System.out.println(String.valueOf(lAttributes.isRegularFile()));
					System.out.println(String.valueOf(lAttributes.isSymbolicLink()));
					System.out.println(String.valueOf(lAttributes.size()));
					break;
				}
			}
		}
	}
	
	/*
	 * Bonus 2
	 */
	public static void removeFile(String iName) {
		Map<String, List<String>> lIndexation = loadIndexation();
		for (Entry<String, List<String>> lEntry : lIndexation.entrySet()) {
			for (String lName : lEntry.getValue()) {
				if (lName.contains(iName)) {
					lEntry.getValue().remove(iName);
				}
			}
		}
	}
}
