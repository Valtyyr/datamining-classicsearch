package search;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import datastore.DataStore;

public class Searcher {

	private Map<String, List<String>> mIndexation;

	public Searcher() {
		mIndexation = DataStore.loadIndexation();
	}
	
	public void launchSearch(String iSequence) {
		if (iSequence.contains("and")) {
			processMultiWord(iSequence);
		} else {
			processMonoWord(iSequence);
		}
	}

	private void processMonoWord(String iWord) {
		List<String> lResult = getSearch(iWord);
		if (lResult != null) {
			for (String lDoc : lResult) {
				System.out.println(lDoc);
			}
		} else {
			System.err.println("Fail to find word --" + iWord + "-- in files");
		}
	}
	
	private void processMultiWord(String iSequence) {
		String lNewSequence = iSequence.replace("and ", "");
		String lTab[] = lNewSequence.split(" ");
		List<String> lFinalResult = getSearch(lTab[0]);
		
		for (int i = 1; i < lTab.length; ++i) {
			List<String> lNextResult = getSearch(lTab[i]);
			if (lNextResult != null) {
				lFinalResult.retainAll(lNextResult);
			} else {
				lFinalResult.clear();
			}
		}
		
		if (lFinalResult.size() == 0) {
			System.err.println("Those words never match together in a same file");
		} else {
			for (String lDoc : lFinalResult) {
				System.out.println(lDoc);
			}
		}
	}

	private List<String> getSearch(String iWord) {
		System.out.println("Looking for " + iWord + "...");
		for (Entry<String, List<String>> lEntry : mIndexation.entrySet()) {
			if (lEntry.getKey().equals(iWord)) {
				return lEntry.getValue();
			}
 		}
		return null;
	}
}
