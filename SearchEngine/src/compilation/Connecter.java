package compilation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

public class Connecter {

	private final Map<String, String> mContents;
	
	public Connecter() {
		mContents = new HashMap<String, String>();
	}

	public Map<String, String> getContents() {
		return mContents;
	}

	public void listFilesInFolder(final String iName) throws IOException {
        File lRoot = new File(iName);
        File[] lList = lRoot.listFiles();
        
        for (File lFile : lList) {
            if (lFile.isDirectory()) {
                listFilesInFolder(lFile.getAbsolutePath());
            }
            else {
                addDataInFile(lFile);
            }
        }
	}
	
	private void addDataInFile(File iFile) throws IOException {
		String lContent = FileUtils.readFileToString(iFile);
		mContents.put(iFile.getAbsolutePath(), lContent);
	}
}
