package compilation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Analyser {

	private final List<String> mRemovableSuffix;
	private final List<String> mRemovablePrefix;
	private final List<String> mRemovablePonctuation;
	
	public Analyser() {
		
		mRemovableSuffix = new ArrayList<String>();
		mRemovableSuffix.add("ly");
		mRemovableSuffix.add("less");
		mRemovableSuffix.add("ion");
		mRemovableSuffix.add("er");
		mRemovableSuffix.add("en");
		mRemovableSuffix.add("able");
		mRemovableSuffix.add("ful");
		mRemovableSuffix.add("ible");
		mRemovableSuffix.add("ness");
		
		mRemovablePrefix = new ArrayList<String>();
		mRemovablePrefix.add("un");
		mRemovablePrefix.add("re");
		mRemovablePrefix.add("mis");
		mRemovablePrefix.add("in");
		mRemovablePrefix.add("im");
		mRemovablePrefix.add("dis");
		
		mRemovablePonctuation = new ArrayList<String>();
		mRemovablePonctuation.add(" ");
		mRemovablePonctuation.add(",");
		mRemovablePonctuation.add("|");
		mRemovablePonctuation.add("^");
		mRemovablePonctuation.add("�");
		mRemovablePonctuation.add("%");
		mRemovablePonctuation.add("�");
		mRemovablePonctuation.add("$");
		mRemovablePonctuation.add(";");
		mRemovablePonctuation.add(".");
		mRemovablePonctuation.add("=");
		mRemovablePonctuation.add(":");
		mRemovablePonctuation.add("?");
		mRemovablePonctuation.add("!");
		mRemovablePonctuation.add("@");
		mRemovablePonctuation.add("<");
		mRemovablePonctuation.add(">");
		mRemovablePonctuation.add(")");
		mRemovablePonctuation.add("(");
		mRemovablePonctuation.add("-");
		mRemovablePonctuation.add("+");
		mRemovablePonctuation.add("*");
		mRemovablePonctuation.add("/");
		mRemovablePonctuation.add("&");
		mRemovablePonctuation.add("#");
		mRemovablePonctuation.add("_");
		mRemovablePonctuation.add("[");
		mRemovablePonctuation.add("]");
		mRemovablePonctuation.add("{");
		mRemovablePonctuation.add("}");
	}

	private List<String> normToken(String iContent) {
		List<String> lWordsInFile = new ArrayList<String>();
		String lNewContent = iContent;
		
		for (int i = 0; i < mRemovablePonctuation.size(); ++i) {
			String lStringToRemove = mRemovablePonctuation.get(i);
			lNewContent = lNewContent.replace(lStringToRemove, " ");
		}
		
		lNewContent = lNewContent.replace("�", "e");
		lNewContent = lNewContent.replace("�", "e");
		lNewContent = lNewContent.replace("�", "e");
		lNewContent = lNewContent.replace("�", "e");
		lNewContent = lNewContent.replace("�", "a");
		lNewContent = lNewContent.replace("�", "a");
		lNewContent = lNewContent.replace("�", "a");
		lNewContent = lNewContent.replace("�", "u");
		lNewContent = lNewContent.replace("�", "u");
		lNewContent = lNewContent.replace("�", "u");
		lNewContent = lNewContent.replace("�", "c");
		lNewContent = lNewContent.replace("�", "i");
		lNewContent = lNewContent.replace("�", "i");
		lNewContent = lNewContent.toLowerCase();
		
		String lTab[] = lNewContent.split(" ");
		lWordsInFile = Arrays.asList(lTab);

		List<String> lFinalWords = new ArrayList<String>();
		
		for (String lWord : lWordsInFile) {
			if (lWord.length() >= 1) {
				lFinalWords.add(lWord);
			}
		}
		
		return lFinalWords;
	}
	
	public void processTokenization(Map<String, String> iContents) {
		Indexer lIndexer = new Indexer();
		for (Entry<String, String> lEntry : iContents.entrySet()) {
			List<String> lWordsInFile = normToken(lEntry.getValue());
			for (String lWord : lWordsInFile) {
				if (!lWord.equals(" ")) {
					for (String lPrefix : mRemovablePrefix) {
						for (int i = -1; (i = lWord.indexOf(lPrefix, i + 1)) != -1;) {
							if (i == 0) {
								lWord.replace(lPrefix, "");
							}
						} 
					}
					
					for (String lSuffix : mRemovableSuffix) {
						for (int i = -1; (i = lWord.indexOf(lSuffix, i + 1)) != -1;) {
							if (i != 0) {
								lWord.replace(lSuffix, "");
							}
						} 
					}
					lIndexer.setIndex(lWord, lEntry.getKey());
				}
			}
		}
		lIndexer.finish();
	}
}
