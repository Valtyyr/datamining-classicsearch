package compilation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import datastore.DataStore;

public class Indexer {
	private final Map<String, List<String>> mIndexation;
	
	public Indexer() {
		mIndexation = new HashMap<String, List<String>>();
	}
	
	public void setIndex(String iWord, String iURL) {
		if (mIndexation.containsKey(iWord)) {
			if (!mIndexation.get(iWord).contains(iURL)) { 
				mIndexation.get(iWord).add(iURL);
			}
		} else {
			List<String> lIndexList = new ArrayList<String>();
			lIndexList.add(iURL);
			mIndexation.put(iWord, lIndexList);
		}
	}

	public void finish() {
		DataStore.saveOnDisk(mIndexation);
	}
}
