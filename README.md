# README #

### Authors ###

Valentin 'Valtyr' Macheret

### Overview ###

This project is an ultra basic search tool for learn how to browse and find data in files.

### How do I get set up? ###

You need :

* A Java Runtime Environment 7.
* A Java Software Development Kit.

### Run guidelines ###

#### Compilation step ####

* Set COMPILATION variable to TRUE in the MainClass class.
* The program found all files in the resources folder.
* After tokenization of each file content, the program stocks word in a map data structure.
* The datastructure is finally serialize.

#### Run step ####

* Set COMPILATION variable to FALSE in the MainClass class.
* You can process research with the launchSearch(string) method.
* The program auto deserialize the map datastructure.